extern crate askama;
extern crate rsass;

use std::fs::OpenOptions;
use std::io::Write;
use std::path::Path;
use std::str;

use rsass::{compile_scss_file, OutputStyle};

fn main() {
    askama::rerun_if_templates_changed();

    match compile_scss_file(Path::new("./assets/scss/akaibu.scss"), OutputStyle::Expanded) {
        Ok(mut css) => {
            match OpenOptions::new().create(true).write(true).truncate(true).open("./templates/partial_css.html") {
                Ok(mut file) => {
                    // strips newline and tabs
                    css.retain(|c| match *c {
                        9u8 | 10u8 | 11u8 | 12u8 | 13u8 => false,
                        _ => true,
                    });
                    let _ = file.write_all(
                        format!(
                            "<style>{}</style>",
                            str::from_utf8(&css).unwrap(),
                        ).as_bytes()
                    );
                },
                Err(e) => {
                    println!("{}", e);
                },
            }
        },
        Err(e) => {
            println!("{}", e);
        },
    }
}