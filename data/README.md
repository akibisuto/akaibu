# Convert.rb

A (simple) tool to convert EPub books into markdown.

## Requires

    - Ruby
    - Pandoc

## Instructions

Place the unzipped EPub folder into the `stories` folder and run `ruby convert.rb`.