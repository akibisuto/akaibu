stories = Dir.open("./stories")

stories.each do |item|
    next if item == '.' or item == '..' or item == '.gitinclude'
    puts item
    Dir.glob("./stories/#{item}/OEBPS/*.html") do |item|
        system("pandoc -s -r html \"#{item}\" -o \"#{item.chomp(".html")}.md\" -t markdown-header_attributes-link_attributes-native_divs-native_spans-smart-native_divs-yaml_metadata_block-pandoc_title_block-all_symbols_escapable-raw_html")
    end
end