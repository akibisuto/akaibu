use std::error::Error as StdError;
use std::fmt;
use std::io;
use std::num::ParseIntError;
use std::string::FromUtf8Error;

use askama_shared::Error as AskamaError;
use cookie::ParseError as CookieError;
use direkuta::prelude::rt::*;
use direkuta::prelude::*;
use encoding::{Encoding, EncoderTrap};
use encoding::all::ISO_8859_1;
use validator::ValidationErrors;
use zxcvbn::ZxcvbnError;

use base64::DecodeError;
use bcrypt::BcryptError;
use postgres::Error as PostgresErrors;
use r2d2::Error as R2D2Error;

use serde_urlencoded::de::Error as SerdeUrlDeError;
use serde_json::Error as JsonError;

#[derive(Debug)]
pub enum Error {
    Int(ParseIntError),
    Io(io::Error),
    Utf8(FromUtf8Error),

    Askama(AskamaError),
    Cookie(CookieError),
    Direkuta(DireError),
    Validator(ValidationErrors),
    Zxcvbn(ZxcvbnError),

    Base64(DecodeError),
    BCrypt(BcryptError),
    Postgres(PostgresErrors),
    R2D2(R2D2Error),

    SerdeUrlDe(SerdeUrlDeError),
    Json(JsonError),

    BufferEmpty,
    SqlEmptyRow,
    SqlGetRow,
}

impl Error {
    #[allow(dead_code)]
    pub(crate) fn dire_error(&self) -> DireError {
        DireError::Other(format!("{}, {:?}", self.description(), self.cause()))
    }

    // https://cryptii.com
    pub(crate) fn dire_res(&self) -> Response {
        Response::new()
            .with_status(500)
            .with_html(format!(r#"<!DOCTYPE html><html><head><title>500 Server Error</title><meta name="viewport" content="width=device-width, initial-scale=1"><style>* {{ margin: 0; padding: 0; box-sizing: border-box; }} html {{ background: #eee; height: 100%; width: 100%; }} body {{ background: #fff; height: 100%; line-height: 1.15; font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif; margin: 0 auto; max-width: 1024px; padding: 0 10px; }} h3, h4 {{ padding-top: 5px; }} h3, h4, p {{ margin-bottom: 5px; }}</style></head><body><h3>500 Server Error</h3><p>Sorry about that, please send the following code to <a href="mailto:example@email.com">example@email.com</a>.</p><h4>Code:</h4><p>{}</p></body></html>"#, 
                match ISO_8859_1.encode(&format!("{} {:?}", self.description(), self.cause()), EncoderTrap::Strict) {
                    Ok(code) => {
                        code.iter().fold(String::new(), |acc, &num| acc + &num.to_string() + " ")
                    },
                    Err(e) => {
                        eprintln!("Error Error {}:", e);
                        String::new()
                    },
                }
            ))
    }

    #[allow(dead_code)]
    pub(crate) fn dire_fut(&self) -> FutureResponse {
        self.dire_res().build()
    }
}

macro_rules! impl_from_error {
    ($f: ty, $e: expr) => {
        impl From<$f> for Error {
            fn from(f: $f) -> Error {
                $e(f)
            }
        }
    };
}

impl_from_error!(ParseIntError, Error::Int);
impl_from_error!(io::Error, Error::Io);
impl_from_error!(FromUtf8Error, Error::Utf8);

impl_from_error!(AskamaError, Error::Askama);
impl_from_error!(CookieError, Error::Cookie);
impl_from_error!(DireError, Error::Direkuta);
impl_from_error!(ValidationErrors, Error::Validator);
impl_from_error!(ZxcvbnError, Error::Zxcvbn);

impl_from_error!(DecodeError , Error::Base64);
impl_from_error!(BcryptError, Error::BCrypt);
impl_from_error!(PostgresErrors, Error::Postgres);
impl_from_error!(R2D2Error, Error::R2D2);

impl_from_error!(SerdeUrlDeError, Error::SerdeUrlDe);
impl_from_error!(JsonError, Error::Json);

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Int(ref err) => write!(f, "Int error: {}", err),
            Error::Io(ref err) => write!(f, "IO error: {}", err),
            Error::Utf8(ref err) => write!(f, "IO error: {}", err),

            Error::Askama(ref err) => write!(f, "Askama error: {}", err),
            Error::Cookie(ref err) => write!(f, "Cookie Parse error: {}", err),
            Error::Direkuta(ref err) => write!(f, "Direkuta error: {}", err),
            Error::Validator(ref err) => write!(f, "Validator error: {}", err),
            Error::Zxcvbn(ref err) => write!(f, "Zxcvbn error: {}", err),

            Error::Base64(ref err) => write!(f, "BCrypt error: {}", err),
            Error::BCrypt(ref err) => write!(f, "BCrypt error: {}", err),
            Error::Postgres(ref err) => write!(f, "Postgres error: {}", err),
            Error::R2D2(ref err) => write!(f, "R2D2 error: {}", err),

            Error::SerdeUrlDe(ref err) => write!(f, "Serde De error: {}", err),
            Error::Json(ref err) => write!(f, "Json error: {}", err),

            Error::BufferEmpty => write!(f, "Url Buffer Empty"),
            Error::SqlEmptyRow => write!(f, "Sql Row Empty"),
            Error::SqlGetRow => write!(f, "Sql Row Get"),
        }
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        match *self {
            Error::Int(ref err) => err.description(),
            Error::Io(ref err) => err.description(),
            Error::Utf8(ref err) => err.description(),

            Error::Askama(ref err) => err.description(),
            Error::Cookie(ref err) => err.description(),
            Error::Direkuta(ref err) => err.description(),
            Error::Validator(ref err) => err.description(),
            Error::Zxcvbn(ref err) => err.description(),

            Error::Base64(ref err) => err.description(),
            Error::BCrypt(ref err) => err.description(),
            Error::Postgres(ref err) => err.description(),
            Error::R2D2(ref err) => err.description(),

            Error::SerdeUrlDe(ref err) => err.description(),
            Error::Json(ref err) => err.description(),

            Error::BufferEmpty => "Url Buffer Empty",
            Error::SqlEmptyRow => "Sql Row Empty",
            Error::SqlGetRow => "Sql Row Get",
        }
    }

    fn cause(&self) -> Option<&StdError> {
        match *self {
            Error::Int(ref err) => Some(err),
            Error::Io(ref err) => Some(err),
            Error::Utf8(ref err) => Some(err),

            Error::Askama(ref err) => Some(err),
            Error::Cookie(ref err) => Some(err),
            Error::Direkuta(ref err) => Some(err),
            Error::Validator(ref err) => Some(err),
            Error::Zxcvbn(ref err) => Some(err),

            Error::Base64(ref err) => Some(err),
            Error::BCrypt(ref err) => Some(err),
            Error::Postgres(ref err) => Some(err),
            Error::R2D2(ref err) => Some(err),

            Error::SerdeUrlDe(ref err) => Some(err),
            Error::Json(ref err) => Some(err),
            _ => None,
        }
    }
}
