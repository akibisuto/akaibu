pub struct Sql {
    query: String,
}

impl Sql {
    pub fn new() -> Self {
        Self {
            query: String::new(),
        }
    }

    pub fn build(self) -> String {
        self.query
    }

    pub fn select(mut self, sel: Select) -> Self {
        self.query.push_str("SELECT ");
        
        self.query.push_str(&sel.build());

        self
    }
}

pub struct Select {
    mark: u8,
    query: String,
}

impl Select {
    pub fn new() -> Self {
        Self {
            mark: 1,
            query: String::new(),
        }
    }

    fn build(self) -> String {
        self.query
    }
    
    pub fn cols(mut self, cols: &[&str]) -> Self {
        for (i, col) in cols.iter().enumerate() {
            self.query.push_str(col);
            
            if i != (cols.len() - 1) {
                self.query.push_str(", ");
            } else {
                self.query.push_str(" ");
            }
        }

        self
    }

    pub fn story(mut self) -> Self {
        self.query.push_str("id, author, name, length, summary, created , updated, rating, progress, origins, tags, chapters, language, warning, followers, favoritors FROM stories ");

        self
    }
    
    #[allow(non_snake_case)]
    pub fn FROM(mut self, fro: &str) -> Self {
        self.query.push_str("FROM ");

        self.query.push_str(fro);

        self.query.push(' ');

        self
    }
    
    #[allow(non_snake_case)]
    pub fn LIMIT(mut self, limit: i32) -> Self {
        self.query.push_str("LIMIT ");

        self.query.push_str(&limit.to_string());

        self
    }
    
    #[allow(dead_code, non_snake_case)]
    pub fn OFFSET(mut self, limit: i32) -> Self {
        self.query.push_str("OFFSET ");

        self.query.push_str(&limit.to_string());

        self
    }

    #[allow(non_snake_case)]
    pub fn WHERE(mut self, key: &str) -> Self {
        if self.mark == 1 {
            self.query.push_str("WHERE ");
        }

        self.query.push_str(key);

        self.query.push_str("=$");

        self.query.push_str(&self.mark.to_string());
        self.mark += 1;

        self
    }
    
    #[allow(non_snake_case)]
    pub fn AND(mut self) -> Self {
        self.query.push_str(" AND ");

        self
    }
}