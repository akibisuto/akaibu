use cookie::Cookie;
use direkuta::prelude::hyper::*;
use direkuta::prelude::rt::*;
use direkuta::prelude::*;

use r2d2::PooledConnection;
use crate::r2d2_postgres::PostgresConnectionManager;

use crate::error;
use crate::models::template;

pub fn match_res(run: impl Fn() -> Result<Response, error::Error>) -> FutureResponse {
    match run() {
        Ok(res) => res,
        Err(e) => e.dire_res(),
    }.build()
}

pub fn askama<
    I: template::pages::Page,
>(
    mut res: Response,
    conn: &PooledConnection<PostgresConnectionManager>,
    c: Option<Cookie>,
    ctx: &mut I,
) -> Result<Response, error::Error> {
    
    if let Some(c) = c {
        if c.name() == "key" {
            ctx.set_user(template::User::cookie(&conn, &c)?);
        }
    }

    res.html(ctx.render()?);

    Ok(res)
}

pub fn askama_cookie<
    I: template::pages::Page,
>(
    mut res: Response,
    conn: &PooledConnection<PostgresConnectionManager>,
    headers: &HeaderMap<HeaderValue>,
    ctx: &mut I,
) -> Result<Response, error::Error> {
    res = askama(res, &conn, cookie(headers)?, ctx)?;

    Ok(res)
}

pub fn cookie(headers: &HeaderMap<HeaderValue>) -> Result<Option<Cookie>, error::Error> {
    if headers.contains_key(header::COOKIE) {
        let c = Cookie::parse(headers.get(header::COOKIE).unwrap().to_str().unwrap())?;
        Ok(Some(c))
    } else {
        Ok(None)
    }
}

fn readable(num: i32) -> String {
    let mut num_str = num.to_string(); 
    let mut s = String::new();
    let mut negative = false;

    let values: Vec<char> = num_str.chars().collect();

    if values[0] == '-' {
        num_str.remove(0);
        negative = true;
    }

    for (i ,char) in num_str.chars().rev().enumerate() {
        if i % 3 == 0 && i != 0 {
            s.insert(0, ',');
        }
        s.insert(0, char);
    }

    if negative {
        s.insert(0, '-');
    }

    s
}

fn word_count(num: &str) -> i32 {
    num.split_whitespace()
        .filter(|s| match *s {
            "---" => false,
            "#" | "##" | "###" | "####" | "#####" | "######" => false,
            _ => true,
        })
        .count() as i32
}