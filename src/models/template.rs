use std::collections::HashMap;

use cookie::Cookie;

use base64::decode;
use chrono::NaiveDate;
use nanoid::custom;
use postgres::rows::Row;
use postgres::stmt::Statement;
use r2d2::PooledConnection;
use crate::r2d2_postgres::PostgresConnectionManager;

use crate::error::Error;
use crate::sql::{Select, Sql};

const NANOID_SAFE: [char; 62] = [
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z',
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
    'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
];

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Chapter {
    pub name: String,
    pub number: i32,
    pub note: Note,
    pub content: Rendered,
}

impl Chapter {
    pub fn id() -> String {
        custom(10, &NANOID_SAFE)
    }

    pub fn sql_select(
        conn: &PooledConnection<PostgresConnectionManager>, 
        story: &str,
        chapter: i32,
    ) -> Result<Chapter, Error> {
        let query = conn.query(
            &Sql::new().select(
                Select::new().cols(&[
                    "name", "length",
                    "note_before_raw", "note_before_rendered",
                    "note_after_raw", "note_after_rendered",
                    "content_raw", "content_rendered",
                ]).FROM("chapters")
                .WHERE("number").AND().WHERE("story")
            ).build(),
            &[&chapter, &story],
        )?;

        if query.is_empty() {
            Err(Error::SqlEmptyRow)
        } else if query.len() == 1 {
            let row = query.get(0);
            let chapter = {
                Chapter {
                    name: row.get(0),
                    number: chapter,
                    note: Note {
                        before: Rendered::from_base64(row.get(2), row.get(3))?,
                        after: Rendered::from_base64(row.get(4), row.get(5))?,
                    },
                    content: Rendered::from_base64(row.get(6), row.get(7))?,
                }
            };

            Ok(chapter)
        } else {
            Err(Error::SqlGetRow)
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Note {
    pub before: Rendered,
    pub after: Rendered,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Rendered {
    pub raw: String,
    pub rendered: String,
}

impl Rendered {
    #[allow(clippy::needless_pass_by_value)]
    fn from_base64(raw: String, rendered: String) -> Result<Rendered, Error> {
        let rendered = Rendered {
            raw: String::from_utf8(decode(&raw)?)?,
            rendered: String::from_utf8(decode(&rendered)?)?,
        };

        Ok(rendered)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Language {
    pub id: String,
    pub name: String,
}

impl Language {
    pub fn id() -> String {
        custom(10, &NANOID_SAFE)
    }

    pub fn sql(
        conn: &PooledConnection<PostgresConnectionManager>, 
        id: &str
    ) -> Result<Language, Error> {
        let query = conn.query(
            "SELECT id, name FROM languages WHERE id=$1",
            &[&id],
        )?;

        if query.is_empty() {
            Err(Error::SqlEmptyRow)
        } else if query.len() == 1 {
            let row = query.get(0);
            Ok(Language {
                id: row.get(0), 
                name: row.get(1),
            })
        } else {
            Err(Error::SqlGetRow)
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Origin {
    pub id: String,
    pub name: String,
}

impl Origin {
    pub fn id() -> String {
        custom(10, &NANOID_SAFE)
    }

    #[allow(dead_code)]
    pub fn sql(
        conn: &PooledConnection<PostgresConnectionManager>, 
        id: &str
    ) -> Result<Origin, Error> {
        let query = conn.query(
            "SELECT id, name FROM origins WHERE id=$1", 
            &[&id],
        )?;

        if query.is_empty() {
            Err(Error::SqlEmptyRow)
        } else if query.len() == 1 {
            let row = query.get(0);
            Ok(Origin {
                id: row.get(0), 
                name: row.get(1),
            })
        } else {
            Err(Error::SqlGetRow)
        }
    }

    pub fn sql_multi(
        conn: &PooledConnection<PostgresConnectionManager>, 
        ids: &[String],
    ) -> Result<Vec<Origin>, Error> {
        let query = conn.query(
            "SELECT id, name FROM origins WHERE id = ANY($1)",
            &[&ids],
        )?;

        let mut origin_vec: Vec<Origin> = Vec::new();

        for row in query.iter() {
            origin_vec.push(Origin {
                id: row.get(0), 
                name: row.get(1),
            })
        }
        
        Ok(origin_vec)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Rating {
    pub name: String,
    pub id: String,
}

impl Rating {
    pub fn id() -> String {
        custom(10, &NANOID_SAFE)
    }

    pub fn sql(
        conn: &PooledConnection<PostgresConnectionManager>, 
        id: &str
    ) -> Result<Rating, Error> {
        let query = conn.query(
            "SELECT id, name FROM ratings WHERE id=$1", 
            &[&id],
        )?;
                
        if query.is_empty() {
            Err(Error::SqlEmptyRow)
        } else if query.len() == 1 {
            let row = query.get(0);
            Ok(Rating {
                id: row.get(0), 
                name: row.get(1),
            })
        } else {
            Err(Error::SqlGetRow)
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Role {
    pub id: String,
    pub name: String,
    pub rank: i32,
    pub color: String,
}

impl Role {
    pub fn id() -> String {
        custom(10, &NANOID_SAFE)
    }

    pub fn sql_id(
        conn: &PooledConnection<PostgresConnectionManager>,
        id: &str,
    ) -> Result<Role, Error> {
        let query = conn.query(
            "SELECT id, name, rank, color FROM roles WHERE id=$1", 
            &[&id],
        )?;

        if query.is_empty() {
            Err(Error::SqlEmptyRow)
        } else if query.len() == 1 {
            let row = query.get(0);
            Ok(Role {
                id: row.get(0), 
                name: row.get(1),
                rank: row.get(2),
                color: row.get(3),
            })
        } else {
            Err(Error::SqlGetRow)
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Story {
    pub id: String,
    pub name: String,
    // Info
    pub author: User,
    pub origins: Vec<Origin>,
    pub created: String,
    pub updated: String,
    pub summary: String,
    pub tags: Vec<Tag>,
    // Cube
    pub rating: Rating,
    pub warning: String,
    pub progress: String,
    // Meta
    pub language: Language,
    pub length: i32,
    pub chapters: HashMap<i32, String>,
    pub chapter_length: HashMap<i32, i32>,
    pub chapter_count: usize,
    pub followers: Vec<String>,
    pub followers_count: usize,
    pub favoritors: Vec<String>,
    pub favoritors_count: usize,
}

pub type StoryRow = (
    String, // Id
    String, // Author
    String, // Name
    HashMap<String, Option<String>>, // Length
    String, // Summary
    NaiveDate, // Created
    NaiveDate, // Updated
    String, // Rating
    bool, // Progress
    Vec<String>, // Origins
    Vec<String>, // Tags
    HashMap<String, Option<String>>, // Chapters
    String, // Language
    bool, // Warning
    Vec<String>, // Followers
    Vec<String>, // Favoritors
);

impl Story {
    pub fn id() -> String {
        custom(10, &NANOID_SAFE)
    }

    pub fn row(row: &Row) -> StoryRow {
        (
            row.get(0), // Id
            row.get(1), // Author
            row.get(2), // Name
            row.get(3), // Length
            row.get(4), // Summary
            row.get(5), // Created
            row.get(6), // Updated
            row.get(7), // Rating
            row.get(8), // Progress
            row.get(9), // Origins
            row.get(10), // Tags
            row.get(11), // Chapters
            row.get(12), // Language
            row.get(13), // Warning
            row.get(14), // Followers
            row.get(15), // Favoritors
        )
    }

    pub fn sql_stmt(
        conn: &PooledConnection<PostgresConnectionManager>,
        stmt: &Statement,
    ) -> Result<Vec<Story>, Error> {
        let query = stmt.query(&[])?;

        let mut story_vec: Vec<Story> = Vec::new();

        for row in query.into_iter() {
            let story: StoryRow = Self::row(&row);

            story_vec.push(Self::sql_row(&conn, story)?);
        }
        
        Ok(story_vec)
    }

    pub fn sql_stmt_id(
        conn: &PooledConnection<PostgresConnectionManager>,
        stmt: &str,
        id: &str,
    ) -> Result<Story, Error> {
        let query = conn.query(
            &stmt,
            &[&id],
        )?;

        if query.is_empty() {
            Err(Error::SqlEmptyRow)
        } else if query.len() == 1 {
            let row = query.get(0);

            let story = Self::sql_row(&conn, Self::row(&row))?;

            Ok(story)
        } else {
            Err(Error::SqlGetRow)
        }
    }

    pub fn sql_row(
        conn: &PooledConnection<PostgresConnectionManager>,
        (
            id, author, name, length, 
            summary, created, updated, rating, progress, 
            origins, tags, chapters, language, 
            warning, followers, favoritors,
        ): StoryRow
    ) -> Result<Story, Error> {
        let chapter_count = chapters.clone().len();
        let length_map: Result<(HashMap<i32, i32>, i32), Error> = {
            let mut map: HashMap<i32, i32> = HashMap::with_capacity(length.len());
            let mut len: i32 = 0;

            for (key, value) in length {
                if let Some(value) = value {
                    let key = key.parse::<i32>()?;
                    let value = value.parse::<i32>()?;
                    len += value;
                    let _ = map.insert(key, value);
                }
            }

            Ok((map, len))
        };

        let chapter_map: Result<HashMap<i32, String>, Error> = {
            let mut map: HashMap<i32, String> = HashMap::with_capacity(chapters.len());

            for (key, value) in chapters {
                if let Some(value) = value {
                    let key = key.parse::<i32>()?;
                    let _ = map.insert(key, value);
                }
            }

            Ok(map)
        };

        let (chapter_length, length) = length_map?;

        let story = Self {
            id, name, summary, length, chapter_length, chapter_count,
            followers_count: followers.len(),
            favoritors_count: favoritors.len(),
            followers, favoritors,
            progress: String::from(if progress {
                "complete"
            } else {
                "in-progress"
            }),
            warning: String::from(if warning {
                "warnings"
            } else {
                "no-warnings"
            }),
            created: created.format("%B %-d, %C%y").to_string(), 
            updated: updated.format("%B %-d, %C%y").to_string(), 
            chapters: chapter_map?,
            author: User::sql(&conn, &author)?,
            rating: Rating::sql(&conn, &rating)?,
            origins: Origin::sql_multi(&conn, &origins)?,
            tags: Tag::sql_multi(&conn, &tags)?,
            language: Language::sql(&conn, &language)?,
        };

        Ok(story)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Tag {
    pub id: String,
    pub name: String,
    pub category: String,
}

impl Tag {
    pub fn id() -> String {
        custom(10, &NANOID_SAFE)
    }

    #[allow(dead_code)]
    pub fn sql(
        conn: &PooledConnection<PostgresConnectionManager>, 
        id: &str
    ) -> Result<Tag, Error> {
        let query =  conn.query(
            "SELECT id, name, category FROM tags WHERE id=$1",
            &[&id],
        )?;
        
        if query.is_empty() {
            Err(Error::SqlEmptyRow)
        } else if query.len() == 1 {
            let row = query.get(0);
            Ok(Tag {
                id: row.get(0), 
                name: row.get(1),
                category: row.get(2),
            })
        } else {
            Err(Error::SqlGetRow)
        }
    }

    pub fn sql_multi(
        conn: &PooledConnection<PostgresConnectionManager>, 
        ids: &[String],
    ) -> Result<Vec<Tag>, Error> {
        let query = conn.query(
            "SELECT id, name, category FROM tags WHERE id = ANY($1)",
            &[&ids],
        )?;

        let mut tag_vec: Vec<Tag> = Vec::new();

        for row in query.into_iter() {
            tag_vec.push(Tag {
                id: row.get(0),
                name: row.get(1),
                category: row.get(2),
            })
        }
        
        Ok(tag_vec)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct User {
    pub id: String,
    pub name: String,
    pub stories: Vec<String>,
    pub bio_raw: String,
    pub bio_rendered: String,
    pub role: Role,
}

impl User {
    pub fn id() -> String {
        custom(10, &NANOID_SAFE)
    }

    pub fn name() -> String {
        custom(10, &NANOID_SAFE)
    }

    pub fn salt() -> String {
        custom(64, &NANOID_SAFE)
    }

    pub fn key() -> String {
        custom(64, &NANOID_SAFE)
    }

    pub fn id_name_salt_key() -> (String, String, String, String) {
        (Self::id(), Self::name(), Self::salt(), Self::key())
    }
    pub fn cookie(
        conn: &PooledConnection<PostgresConnectionManager>, 
        cookie: &Cookie
    ) -> Result<User, Error> {
        let query = conn.query(
            "SELECT id, name, stories, bio_raw, bio_rendered, role FROM users WHERE key=$1",
            &[&cookie.value().to_string()],
        )?;
        
        if query.is_empty() {
            Err(Error::SqlEmptyRow)
        } else if query.len() == 1 {
            let row = query.get(0);
            Ok(User {
                id: row.get(0), 
                name: row.get(1),
                stories: row.get(2),
                bio_raw: row.get(3),
                bio_rendered: row.get(4),
                role: Role::sql_id(&conn, &row.get::<usize, String>(5))?,
            })
        } else {
            Err(Error::SqlGetRow)
        }
    }

    pub fn sql(
        conn: &PooledConnection<PostgresConnectionManager>, 
        id: &str
    ) -> Result<User, Error> {
        let query = conn.query(
            "SELECT id, name, stories, bio_raw, bio_rendered, role FROM users WHERE id=$1", 
            &[&id],
        )?;

        if query.is_empty() {
            Err(Error::SqlEmptyRow)
        } else if query.len() == 1 {
            let row = query.get(0);
            Ok(User {
                id: row.get(0), 
                name: row.get(1),
                stories: row.get(2),
                bio_raw: row.get(3),
                bio_rendered: row.get(4),
                role: Role::sql_id(&conn, &row.get::<usize, String>(5))?,
            })
        } else {
            Err(Error::SqlGetRow)
        }
    }
}

pub mod pages {
    use askama::Template;

    use super::*;

    pub trait Page: Template {
        fn set_user(&mut self, user: User);
    }

    macro_rules! impl_page {
        ($f: ty) => {
            impl Page for $f {
                fn set_user(&mut self, user: User) {
                    self.user = Some(user);
                }
            }
        };
    }

    #[derive(Template)]
    #[template(path = "index.html")]
    pub struct PageIndex {
        pub stories: Vec<Story>,
        pub user: Option<User>,
    }
    impl_page!(PageIndex);

    #[derive(Template)]
    #[template(path = "stories.html")]
    pub struct PageStories {
        pub stories: Vec<Story>,
        pub user: Option<User>,
    }
    impl_page!(PageStories);

    #[derive(Template)]
    #[template(path = "story.html")]
    pub struct PageStory {
        pub story: Story,
        pub user: Option<User>,
    }
    impl_page!(PageStory);

    #[derive(Template)]
    #[template(path = "chapter.html")]
    pub struct PageChapter {
        pub chapter: Chapter,
        pub story: Story,
        pub user: Option<User>,
    }
    impl_page!(PageChapter);

    #[derive(Template)]
    #[template(path = "login.html")]
    pub struct PageLogin {
        pub errors: Vec<String>,
        pub messages: Vec<String>,
        pub user: Option<User>,
    }
    impl_page!(PageLogin);
}