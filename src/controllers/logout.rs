use std::sync::Arc;

use cookie::Cookie;
use direkuta::prelude::hyper::*;
use direkuta::prelude::rt::*;
use direkuta::prelude::*;

use chrono::Duration;

#[allow(clippy::needless_pass_by_value)]
pub fn get(_: Request, _s: Arc<State>, _: Capture) -> FutureResponse {
    Response::new()
        .with_headers(headermap! {
            header::LOCATION => "http://10.0.0.25:8205/",
            header::SET_COOKIE => &Cookie::build("key", "deleted")
                .path("/").max_age(Duration::seconds(0)).http_only(true)
                .finish().to_string()
        }).with_status(302).build()
}