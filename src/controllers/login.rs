use std::sync::Arc;

use askama::Template;
use cookie::Cookie;
use direkuta::prelude::hyper::*;
use direkuta::prelude::rt::*;
use direkuta::prelude::*;
use serde_urlencoded::from_bytes;
use validator::{Validate, ValidationError};
use zxcvbn::zxcvbn;

use bcrypt::{DEFAULT_COST, hash as bcrypt_hash, verify as bcrypt_verify};
use chrono::Duration;
use r2d2::{Pool, PooledConnection};
use crate::r2d2_postgres::PostgresConnectionManager;

use crate::config;
use crate::error;
use crate::models::template;

#[allow(clippy::needless_pass_by_value)]
pub fn get(_: Request, _s: Arc<State>, _: Capture) -> FutureResponse {
    let ctx = template::pages::PageLogin {
        errors: vec![],
        messages: vec![],
        user: None,
    };
    Response::new()
        .with_body(
            match ctx.render() {
                Ok(t) => t,
                Err(e) => {
                    use std::error::Error;
                    format!("Tera Render Error: {} {:?}", e.description(), e.cause())
                },
            },
        ).build()
}

#[derive(Debug, Deserialize, Serialize)]
enum Category {
    Create,
    Login,
}

#[derive(Debug, Deserialize, Serialize, Validate)]
struct Req {
    #[validate(email)]
    email: String,
    #[validate(length(min = "8"), custom = "validate_password")]
    password: String,
    category: Category,
}

impl Default for Req {
    fn default() -> Self {
        Self {
            email: String::new(),
            password: String::new(),
            category: Category::Create,
        }
    }
}

pub fn post(r: Request, s: Arc<State>, _: Capture) -> FutureResponse {
    Box::new(
        r.into_body()
            .concat2()
            .from_err::<DireError>()
            .and_then(move |buffer| {
                let wrap = || -> Result<Res, error::Error> {
                    if !buffer.is_empty() {
                        let en = from_bytes::<Req>(&buffer)?;
                        match en.validate() {
                            Ok(_) => {
                                let pool = s.get_err::<Pool<PostgresConnectionManager>>()?;
                                let conn = pool.clone().get()?;

                                let res = match en.category {
                                    Category::Create => post_create,
                                    Category::Login => post_login,
                                }(&s, &conn, &en)?;

                                Ok(res)
                            },
                            Err(_) =>{
                                let ctx = template::pages::PageLogin {
                                    errors: vec![
                                        "Password does not meet requirements. (more than 8 characters)".to_string(),
                                    ],
                                    messages: vec![
                                        "Add another word or two. Uncommon words are better.".to_string(),
                                        "Avoid dates and years that are associated with you.".to_string(),
                                        "Avoid repeated words and characters.".to_string(),
                                        "Avoid sequences.".to_string(),
                                    ],
                                    user: None,
                                };

                                let res = Response::new()
                                    .with_body(ctx.render()?)
                                    .into_hyper();

                                Ok(res)
                            },
                        }
                    } else {
                        Err(error::Error::BufferEmpty)
                    }
                };

                future::ok(match wrap() {
                    Ok(res) => res,
                    Err(e) => e.dire_res().into_hyper(),
                })
            }),
    )
}

fn post_create(
    s: &Arc<State>, 
    conn: &PooledConnection<PostgresConnectionManager>, 
    en: &Req
) -> Result<Res, error::Error> {
    let cfg = s.get_err::<config::Configuration>()?;

    let query = conn.query(
        "SELECT salt, hash, key FROM users WHERE email=$1",
        &[&en.email.clone()],
    )?;

    if query.is_empty() {
        let (id, name, salt, key) = template::User::id_name_salt_key();
        let hash = bcrypt_hash(&format!(
            "{}-{}-{}", 
            salt, en.password, cfg.database.salt,
        ), DEFAULT_COST)?;

        let _ =conn.execute(
            "INSERT INTO users (id, name, email, hash, salt, key, stories) VALUES ($1, $2, $3, $4, $5, $6, $7)",
            &[&id, &name, &en.email.clone(), &hash, &salt, &key, &"[]".to_string()],
        )?;

        Ok(Response::new()
            .with_headers(headermap! {
                header::LOCATION => "http://10.0.0.25:8205/",
                header::SET_COOKIE => &Cookie::build("key", key)
                    .path("/").max_age(Duration::days(10)).http_only(true)
                    .finish().to_string()
            })
            .with_status(302)
            .into_hyper())
    } else if query.len() == 1 {
        let ctx = template::pages::PageLogin {
            errors: vec![
                "An account already uses this email".to_string(),
            ],
            messages: vec![],
            user: None,
        };

        let res = Response::new()
            .with_body(ctx.render()?)
            .into_hyper();
            
        Ok(res)
    } else {
        Err(error::Error::SqlGetRow)
    }
}

fn post_login(
    s: &Arc<State>, 
    conn: &PooledConnection<PostgresConnectionManager>, 
    en: &Req
) -> Result<Res, error::Error> {
    let cfg = s.get_err::<config::Configuration>()?;

    let query = conn.query(
        "SELECT salt, hash, key FROM users WHERE email=$1",
        &[&en.email.clone()],
    )?;

    if query.is_empty() {
        let ctx = template::pages::PageLogin {
            errors: vec![
                "Account not found".to_string(),
            ],
            messages: vec![],
            user: None,
        };

        Ok(Response::new()
            .with_body(ctx.render()?)
            .into_hyper())
    } else if query.len() == 1 {
        let row = query.get(0);
        let (salt, hash, key): (String, String, String) = {
            (row.get(0), row.get(1), row.get(2))
        };

        let correct = bcrypt_verify(&format!(
            "{}-{}-{}", 
            salt, en.password, cfg.database.salt,
        ), &hash)?;

        if correct {
            Ok(Response::new()
                .with_headers(headermap! {
                    header::LOCATION => "http://10.0.0.25:8205/",
                    header::SET_COOKIE => &Cookie::build("key", key)
                        .path("/").max_age(Duration::days(10)).http_only(true)
                        .finish().to_string(),
                })
                .with_status(302)
                .into_hyper())
        } else {
            Ok(Response::new()
                .with_body(format!("{:?}", en))
                .into_hyper())
        }
    } else {
        Err(error::Error::SqlGetRow)
    }
}

fn validate_password(password: &str) -> Result<(), ValidationError> {
    match zxcvbn(password, &[]) {
        Ok(e) => {
            if e.score < 3 {
                Err(ValidationError::new("horrible password"))
            } else {
                Ok(())
            }
        }
        Err(_) => Err(ValidationError::new("zxcvbn error")),
    }
}
