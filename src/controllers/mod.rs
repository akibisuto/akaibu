use std::sync::Arc;

use direkuta::prelude::rt::*;
use direkuta::prelude::*;

use r2d2::Pool;
use crate::r2d2_postgres::PostgresConnectionManager;

use crate::models::template;
use crate::sql::{Select, Sql};

mod login;
mod logout;

mod groups;
mod stories;
mod users;

pub fn build(r: &mut ::direkuta::Router) {
    r.get("/logout", logout::get);
    r.path("/login", |r| {
        r.get("", login::get);
        r.post("", login::post);
    });

    r.path("/groups", groups::build);

    r.path("/stories", stories::build);

    r.path("/users", users::build);

    r.get("/", get);
}

#[allow(clippy::needless_pass_by_value)]
pub fn get(r: Request, s: Arc<State>, _c: Capture) -> FutureResponse {
    crate::utils::match_res(|| {
        let res = Response::new();

        let conn = s.get_err::<Pool<PostgresConnectionManager>>()?.clone().get()?;

        let mut ctx = template::pages::PageIndex {
            stories: template::Story::sql_stmt(
                &conn, 
                &conn.prepare(
                    &Sql::new().select(
                        Select::new().story().LIMIT(3)
                    ).build()
                )?
            )?,
            user: None,
        };

        let res = crate::utils::askama_cookie(res, &conn, r.headers(), &mut ctx)?;

        Ok(res)
    })
}
