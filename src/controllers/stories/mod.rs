use std::sync::Arc;

use direkuta::prelude::rt::*;
use direkuta::prelude::*;

use r2d2::Pool;
use crate::r2d2_postgres::PostgresConnectionManager;

use crate::error;
use crate::models::template;
use crate::sql::{Select, Sql};

mod new;
mod search;

mod story;

pub fn build(r: &mut ::direkuta::Router) {
    r.get("/", get);
    r.get("/<page:([0-9]{4}|[0-9]{3}|[0-9]{2}|[0-9]{1})>", get);

    r.path("/new", |r| {
        r.get("/", new::get);
        r.post("/", new::post);
    });

    r.path("/search", |r| {
        r.get("/", search::get);
        r.post("/", search::post);
    });

    r.path("/<story:(.*)>", story::build);
}

#[allow(clippy::needless_pass_by_value)]
pub fn get(r: Request, s: Arc<State>, c: Capture) -> FutureResponse {
    crate::utils::match_res(|| {
        let page = c.try_get_parse::<i32>("page").unwrap_or(0i32);
        let res = Response::new();

        let conn = s.get_err::<Pool<PostgresConnectionManager>>()?.clone().get()?;

        let query = conn.query(
            "SELECT count(*) AS estimate FROM stories TABLESAMPLE SYSTEM (1);",
            &[],
        )?;

        if query.is_empty() {
            Err(error::Error::SqlEmptyRow)
        } else if query.len() == 1 {
            let row = query.get(0);
            let _count: i64 = row.get(0);
            // println!("count {}", count);

            let mut ctx = template::pages::PageStories {
                stories: template::Story::sql_stmt(
                    &conn, 
                    &conn.prepare(&Sql::new().select(
                        Select::new().story().LIMIT(10)
                            .OFFSET(if page == 0 { page } else { page - 1 } * 10)
                    ).build())?,
                )?,
                user: None,
            };

            let res = crate::utils::askama_cookie(res, &conn, r.headers(), &mut ctx)?;

            Ok(res)
        } else {
            Err(error::Error::SqlGetRow)
        }
    })
}