use std::sync::Arc;

use direkuta::prelude::rt::*;
use direkuta::prelude::*;

use r2d2::Pool;
use crate::r2d2_postgres::PostgresConnectionManager;

use crate::models::template;
use crate::sql::{Select, Sql};

pub fn build(r: &mut ::direkuta::Router) {
    r.get("", get);
}

// TODO:
//      Select by id statement
//      Select chapter by HashMap key
#[allow(clippy::needless_pass_by_value)]
pub fn get(r: Request, s: Arc<State>, c: Capture) -> FutureResponse {
    crate::utils::match_res(|| {
        let res = Response::new();

        let conn = s.get_err::<Pool<PostgresConnectionManager>>()?.clone().get()?;

        let mut ctx = template::pages::PageChapter {
            chapter: template::Chapter::sql_select(
                &conn, 
                &c.get("story").trim().trim_right_matches('/').to_string(),
                c.get("chapter").trim().trim_right_matches('/').parse::<i32>()?,
            )?,
            story: template::Story::sql_stmt_id(
                &conn, 
                &Sql::new().select(
                    Select::new().story().WHERE("id")
                ).build(),
                &c.get("story").trim().trim_right_matches('/').to_string(),
            )?,
            user: None,
        };

        let res = crate::utils::askama_cookie(res, &conn, r.headers(), &mut ctx)?;

        Ok(res)
    })
}