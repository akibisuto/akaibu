use std::sync::Arc;

use direkuta::prelude::rt::*;
use direkuta::prelude::*;

use r2d2::Pool;
use crate::r2d2_postgres::PostgresConnectionManager;

use crate::models::template;
use crate::sql::{Select, Sql};

mod chapter;
mod settings;

pub fn build(r: &mut ::direkuta::Router) {
    r.path("/settings", |r| {
        r.get("/", settings::get);
        r.post("/", settings::post);
        r.put("/", settings::put);
    });

    r.path("/<chapter:(.*)>", chapter::build);

    r.get("/", get);
}

#[allow(clippy::needless_pass_by_value)]
pub fn get(r: Request, s: Arc<State>, c: Capture) -> FutureResponse {
    crate::utils::match_res(|| {
        let res = Response::new();

        let conn = s.get_err::<Pool<PostgresConnectionManager>>()?.clone().get()?;

        let mut ctx = template::pages::PageStory {
            story: template::Story::sql_stmt_id(
                &conn, 
                &Sql::new().select(
                    Select::new().cols(&[
                        "id", "author", "name", "length",
                        "summary", "updated", "rating", "progress",
                        "origins", "tags", "chapters", "language",
                        "warning",
                    ]).FROM("stories").WHERE("id")
                ).build(),
                &c.get("story").trim().trim_right_matches('/').to_string(),
            )?,
            user: None,
        };

        let res = crate::utils::askama_cookie(res, &conn, r.headers(), &mut ctx)?;

        Ok(res)
    })
}