use std::sync::Arc;

use direkuta::prelude::rt::*;
use direkuta::prelude::*;

#[allow(clippy::needless_pass_by_value)]
pub fn get(_r: Request, _s: Arc<State>, _c: Capture) -> FutureResponse {
    Response::new()
        .with_body("search")
        .build()
}

#[allow(clippy::needless_pass_by_value)]
pub fn post(_r: Request, _s: Arc<State>, _c: Capture) -> FutureResponse {
    Response::new()
        .build()
}