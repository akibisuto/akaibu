#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Server {
    pub host: String,
    pub port: String,
}