#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Database {
    pub salt: String,
    #[serde(default = "Database::default_url")]
    pub url: String,
}

impl Database {
    pub fn default_url() -> String {
        String::from("postgres://akaibu:akaibu@localhost:5432/akaibu")
    }
}