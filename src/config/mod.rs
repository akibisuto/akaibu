mod database;
mod server;

use std::error::Error;
use std::fs::File;
use std::io::Read;

use serde_yaml::from_str;

use self::database::Database;
use self::server::Server;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Configuration {
    pub database: Database,
    pub server: Server,
}

impl Configuration {
    pub fn load() -> Self {
        match File::open("config.yaml") {
            Ok(mut file) => {
                let mut buff = String::new();
                match file.read_to_string(&mut buff) {
                    Ok(_) => {
                        match from_str::<Self>(&buff) {
                            Ok(config) => config,
                            Err(e) => {
                                eprintln!("[{:>6}] Yaml File Format Error: {} {:?}", "Error", e.description(), e.cause());
                                ::std::process::exit(-1);
                            },
                        }
                    },
                    Err(e) => {
                        eprintln!("[{:>6}] Yaml File Buffer Error: {} {:?}", "Error", e.description(), e.cause());
                        ::std::process::exit(-1);
                    },
                }
            },
            Err(e) => {
                eprintln!("[{:>6}] Yaml File Open Error: {} {:?}", "Error", e.description(), e.cause());
                ::std::process::exit(-1);
            },
        }
    }
}