use direkuta::prelude::{Middle, Request};

use chrono::prelude::*;

pub struct Log {}

impl Log {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Middle for Log {
    #[inline]
    fn run(&self, req: &mut Request) {
        println!(
            "[{:>6}] {} `{}`",
            req.method().as_ref(),
            Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
            req.uri()
        );
    }
}

impl Default for Log {
    fn default() -> Self {
        Self {}
    }
}