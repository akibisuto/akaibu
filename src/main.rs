#![feature(tool_lints)]

// Web
#[macro_use]
extern crate askama;
extern crate askama_shared;
extern crate cookie;
#[macro_use]
extern crate direkuta;
extern crate encoding;
extern crate serde_urlencoded;
extern crate validator;
#[macro_use]
extern crate validator_derive;
extern crate zxcvbn;

// Database
extern crate base64;
extern crate bcrypt;
extern crate chrono;
extern crate nanoid;
extern crate postgres;
extern crate postgres_array;
extern crate postgres_shared;
extern crate r2d2;

// Content
extern crate ammonia;
extern crate comrak;

// Utils
extern crate futures;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate serde_yaml;

use direkuta::prelude::*;

mod config;
mod controllers;
mod error;
mod middle;
mod models;
mod r2d2_postgres;
mod sql;
mod utils;

const TABLES: &[(&str, &str); 8] = &[(
    "chapters",
    include_str!("../sql/chapters.sql"),
), (
    "languages",
    include_str!("../sql/languages.sql"),
), (
    "origins",
    include_str!("../sql/origins.sql"),
), (
    "ratings",
    include_str!("../sql/ratings.sql"),
), (
    "roles",
    include_str!("../sql/roles.sql"),
), (
    "stories",
    include_str!("../sql/stories.sql"),
), (
    "tags",
    include_str!("../sql/tags.sql"),
), (
    "users",
    include_str!("../sql/users.sql"),
)];

fn main() {
    let cfg = config::Configuration::load();

    let database_url = cfg.clone().database.url;
    let database = {
        use std::error::Error;

        match r2d2_postgres::PostgresConnectionManager::new(
            database_url, 
            r2d2_postgres::TlsMode::None
        ) {
            Ok(manager) => {
                match r2d2::Pool::new(manager) {
                    Ok(p) => p,
                    Err(e) => {
                        eprintln!("[{:>6}] R2D2 Pool Error: {} {:?}", "Error", e.description(), e.cause());
                        ::std::process::exit(-1);
                    }
                }
            },
            Err(e) => {
                eprintln!("[{:>6}] R2D2 Pool Error: {} {:?}", "Error", e.description(), e.cause());
                ::std::process::exit(-1);
            },
        }
    };

    {
        use std::error::Error;
        let pool = database.clone();

        match pool.get() {
            Ok(conn) => {
                for (name, sql) in TABLES {
                    match conn.execute(sql, &[]) {
                        Ok(_) => {}
                        Err(e) => {
                            eprintln!(
                                "[{:>6}] {{{}}} Postgres Execute Error: {} {:?}",
                                "Error",
                                name,
                                e.description(),
                                e.cause(),
                            );
                        }
                    };
                }
            }
            Err(e) => {
                eprintln!("[{:>6}] R2D2 Pool Get Error: {} {:?}", "Error", e.description(), e.cause());
            }
        }
    }

    Direkuta::new()
        .middle(middle::Log::new())
        .state(cfg.clone())
        .state(database)
        .route(controllers::build)
        .run(&format!("{}:{}", cfg.server.host, cfg.server.port));
}
