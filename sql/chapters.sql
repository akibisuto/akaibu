CREATE TABLE
IF NOT EXISTS
    chapters (
        id
            VARCHAR(10) -- Used as a reference
            NOT NULL
            PRIMARY KEY,
        number
            INTEGER -- Chapter number
            NOT NULL,
        name
            TEXT -- Title of the chapter
            NOT NULL,
        length
            INTEGER -- Word count
            NOT NULL,
        story
            VARCHAR(10) -- Id of parent story
            NOT NULL,
        note_before_raw
            TEXT -- Base64 encoded raw markdown
            NOT NULL,
        note_before_rendered
            TEXT -- Base64 encoded rendered markdown
            NOT NULL,
        note_after_raw
            TEXT -- Base64 encoded raw markdown
            NOT NULL,
        note_after_rendered
            TEXT -- Base64 encoded rendered markdown
            NOT NULL,
        content_raw
            TEXT -- Base64 encoded raw markdown
            NOT NULL,
        content_rendered
            TEXT -- Base64 encoded rendered markdown
            NOT NULL
    );