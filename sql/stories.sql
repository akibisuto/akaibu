CREATE TABLE 
IF NOT EXISTS 
    stories (
        id
            VARCHAR(10) -- Used as a reference
            NOT NULL 
            PRIMARY KEY,
        author
            VARCHAR(10) -- Reference to user id
            NOT NULL,
        name
            TEXT -- The title of the story
            NOT NULL,
        length
            HSTORE -- Like the chapter but with the length of each
            NOT NULL,
        summary
            TEXT -- Story summary
            NOT NULL,
        created
            DATE -- The day the story was created
            NOT NULL,
        updated
            DATE -- The day the story was updated
            NOT NULL,
        language
            VARCHAR(10) -- Reference to language id
            NOT NULL,
        rating
            VARCHAR(10) -- Reference to rating id
            NOT NULL,
        warning
            BOOLEAN -- If the story used warnings
            NOT NULL,
        progress
            BOOLEAN -- If the story is completed
            NOT NULL,
        origins
            VARCHAR(10)[] -- Array of references to origin ids
            NOT NULL,
        tags
            VARCHAR(10)[] -- Array of references to tag ids
            NOT NULL,
        chapters
            HSTORE -- Object of references to tag ids
            NOT NULL,
        followers
            VARCHAR(10)[] -- Array of references to user ids
            NOT NULL,
        favoritors
            VARCHAR(10)[] -- Array of references to user ids
            NOT NULL
    );