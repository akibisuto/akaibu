CREATE TABLE
IF NOT EXISTS
    origins (
        id
            VARCHAR(10) -- Used as a reference
            NOT NULL
            PRIMARY KEY,
        name
            TEXT -- Origin name
            NOT NULL,
        category
            TEXT -- Origin category
            NOT NULL
    );