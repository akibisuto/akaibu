CREATE TABLE 
IF NOT EXISTS 
    users (
        id
            VARCHAR(10) -- Used as a reference
            NOT NULL
            PRIMARY KEY,
        name
            TEXT -- User name
            NOT NULL,
        email
            TEXT -- User email
            NOT NULL
            UNIQUE,
        hash
            TEXT -- User password, salted and hashed
            NOT NULL,
        salt
            VARCHAR(64) -- User salt
            NOT NULL,
        key
            VARCHAR(64) -- Private user key
            NOT NULL,
        stories
            VARCHAR(10)[] -- Array of references to story ids
            NOT NULL,
        bio_raw
            TEXT -- Raw markdown bio of the user
            NOT NULL,
        bio_rendered
            TEXT -- Rendered markdown bio of the user
            NOT NULL,
        role
            VARCHAR(10) -- Reference to role id
            NOT NULL
    );