CREATE TABLE
IF NOT EXISTS
    ratings (
        id
            VARCHAR(10) -- Used as a reference
            NOT NULL
            PRIMARY KEY,
        name
            TEXT
            NOT NULL
    );