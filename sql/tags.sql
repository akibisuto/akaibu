CREATE TABLE
IF NOT EXISTS
    tags (
        id
            VARCHAR(10) -- Used as a reference
            NOT NULL
            PRIMARY KEY,
        name
            TEXT -- Tag name
            NOT NULL,
        category
            TEXT -- Tag category
            NOT NULL
    );