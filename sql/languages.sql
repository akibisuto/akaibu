CREATE TABLE
IF NOT EXISTS
    languages (
        id
            VARCHAR(10) -- Used as a reference
            NOT NULL
            PRIMARY KEY,
        name
            TEXT -- Language
            NOT NULL
    );