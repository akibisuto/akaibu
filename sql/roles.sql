CREATE TABLE
IF NOT EXISTS
    roles (
        id
            VARCHAR(10) -- Used as a reference
            NOT NULL
            PRIMARY KEY,
        name
            TEXT -- Name of the role
            NOT NULL,
        rank
            INTEGER -- Rank of role, the lower the higher the rank
            NOT NULL,
        color
            VARCHAR(6)
            NOT NULL
    );